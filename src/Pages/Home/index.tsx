import React, { useState, useEffect } from 'react'

import { Hello } from "@/components/Hello";
import {
  LinearProgress,
} from "@material-ui/core";

export const Home = ()=>{
  
  let [ status, setStatus ] = useState({ loading:true, who: '' })
  
  useEffect(()=>{
    setTimeout(()=>{
      setStatus({
        loading: false,
        who: 'world'
      })
    },2000)
  })
  
  if(status.loading){
    return <LinearProgress />
  }
  return (
    <Hello who={ status.who } />
  )
}

export default Home
