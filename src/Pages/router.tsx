import React from 'react'
import { Switch, Route, Redirect, } from 'react-router'
import loadable from "@loadable/component";

import NotFound from './NotFound'


export const Routes: [string, any][] = [
  ['/', () => <Redirect to="/home" /> ],
  ['/home', loadable(()=>import('./Home')) ],
  ['/404', NotFound ],
]

export interface State {
}

export const Router = ()=>{
  const RouteComponents = Routes.map(([path, Component]) => {
    return <Route exact={path==='/'} key={path} path={path} component={ Component } />
  })
  return (
    <Switch>
      {RouteComponents}
      <Route component={NotFound} />
    </Switch>
  )
}
