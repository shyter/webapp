import axios, { AxiosInstance, AxiosRequestConfig, AxiosPromise, } from "axios";

export class GraphQL {
  client:AxiosInstance
  constructor(url:string,config:AxiosRequestConfig={}){
    this.client = axios.create({
      baseURL: url,
      ...config,
    })
  }

  public async __request(
    data:{ [key:string]:any, query:string, variables?: any,}, 
    config:AxiosRequestConfig={}
  ){
    config = Object.assign({},config,{
      method: 'POST',
      data,
    })
    let result =  await this.client.request<{data:any,errors:{message:string}[]}>(config)
    return result.data
  }
  public query<T,Y={}>(
    query:string, 
    variables:Y={} as any,
    config:AxiosRequestConfig={}
  ):AxiosPromise<T>{
    return this.__request({ query, variables, },config) as any
  }
  public mutate<T,Y={}>(
    mutation:string, 
    variables:Y={} as any,
    config:AxiosRequestConfig={}
  ):AxiosPromise<T> {
    return this.__request({ query: mutation, variables, },config) as any
  }

}

export default GraphQL
