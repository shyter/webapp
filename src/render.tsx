import './dynamic-import-fix'
import *as React from "react";
import *as ReactDOM from "react-dom";
import { App } from "./app";
import { BrowserRouter } from "react-router-dom";
import { CssBaseline } from "@material-ui/core";

const renderApp = ()=>{
  ReactDOM.render(
    (
      <>
      <CssBaseline/>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
      </>
    ),
    document.getElementById('app')
  )
}

renderApp()

declare var module:any
if(module.hot){
  module.hot.accept('./app',()=>{
    renderApp()
  })
}
