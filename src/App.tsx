/// <reference path="./env.d.ts" />
import * as React from 'react';
import { Router } from "./pages/router";
import { SnackbarProvider } from "notistack";

export type Props = {}

export interface State {
}

export const App = ()=> {
  return (
    <SnackbarProvider>
      <Router />
    </SnackbarProvider>
  );
}

export default App;

