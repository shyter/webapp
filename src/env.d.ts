
export let es6:string

declare global {
  interface Process {
    env: {
      [k:string]:string
      APIEndpoint: string
      NODE_ENV: string
    }
  }
  var process:Process
}
