
import { GraphQL } from "@/classes/GraphQL";
export { Query, Mutation } from "./GraphQL";
import { useState, useEffect } from "react";
import { APIEndpoint } from './config'

export const client = new GraphQL(APIEndpoint)

export interface GraphqlError {
  message: string
}

export type GraphqlErrors = GraphqlError[]

export interface Params<V=object> {
  variables?: V, 
  headers?: { [k:string]:string }
}

export type useResp<T> = {
  loading: boolean,
  data:T,
  errors: GraphqlErrors,
}


export const useQuery = <T=object,V=object>(query:string,variables:Params<V>={}):useResp<T>=>{

  let [ data, setData ] = useState<useResp<T>>({
    loading: true,
    data: null as any,
    errors: [],
  })
  
  useEffect(()=>{
    
    client.__request({ query, ...variables, })
    .then(resp=>{
      let data:useResp<T> = {
        loading: false,
        data: resp.data,
        errors: resp.errors || [],
      }
      setData(data)
    })
    
  },[
    query,
    // 得是同一个对象才不会重复请求，所以序列化一下转成字符串
    JSON.stringify(variables)
  ])
  
  return data

}

export const useMutate = useQuery
