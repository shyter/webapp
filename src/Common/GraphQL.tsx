export type Maybe<T> = T | null;

// ====================================================
// Types
// ====================================================

export interface Query {
  hello: string;
}

export interface Mutation {
}

export interface IntrospectionResultData {
  __schema: {
    types: {
      kind: string;
      name: string;
      possibleTypes: {
        name: string;
      }[];
    }[];
  };
}

const result: IntrospectionResultData = {
  __schema: {
    types: []
  }
};

export default result;
