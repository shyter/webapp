
import React, { useState } from 'react'

import { Button } from "@material-ui/core";
import { useSnackbar } from 'notistack';

export interface Props {
  who: string
}

export const Hello:React.StatelessComponent<Props> = (props)=>{
  
  let [ count, setCount ] = useState(0)
  let { enqueueSnackbar } = useSnackbar()
  
  const onClick = ()=>{
    let newCount = count + 1
    setCount(newCount)
    enqueueSnackbar(`hello ${newCount}`)
  }
  
  return (
    <Button onClick={ onClick }>
      hello {props.who}
    </Button>
  )
  
}
